package br.com.collection.controller;

import br.com.collection.dto.ProdutoDto;
import br.com.collection.service.ProdutoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Produto", description = " ")
@RestController
@RequestMapping("/produto")
public class ProdutoController {

    private final ProdutoService produtoService;

    @Autowired
    public ProdutoController(ProdutoService produtoService) {
        this.produtoService = produtoService;
    }

    @GetMapping("/findProduto/{codigoDeBarra}")
    public ProdutoDto findProdutoByCodigoDeBarra(@PathVariable(value = "codigoDeBarra") @ApiParam Long codigoDeBarra){
        return produtoService.findProdutoByCodigoDeBarra(codigoDeBarra);
    }

}
