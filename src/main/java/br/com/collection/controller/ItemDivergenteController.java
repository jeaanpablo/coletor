package br.com.collection.controller;

import br.com.collection.dto.ItemDivergenteDto;
import br.com.collection.service.ItemDivergenteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "Item Divergente", description = " ")
@RestController
@RequestMapping("/itemDivergente")
public class ItemDivergenteController {

    private final ItemDivergenteService itemDivergenteService;

    @Autowired
    public ItemDivergenteController(ItemDivergenteService itemDivergenteService) {
        this.itemDivergenteService = itemDivergenteService;
    }


    @GetMapping("/findItemDivergenteByRomaneio/{codigoRomaneio}")
    public List<ItemDivergenteDto> findItemDivergenteByRomaneio(@PathVariable(value = "codigoRomaneio") @ApiParam Long codigoRomaneio){
        return itemDivergenteService.findItemDivergenteByRomaneio(codigoRomaneio);
    }
}
