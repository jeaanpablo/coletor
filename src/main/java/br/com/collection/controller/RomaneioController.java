package br.com.collection.controller;

import br.com.collection.dto.RomaneioDto;
import br.com.collection.service.RomaneioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Romaneio", description = " ")
@RestController
@RequestMapping("/romaneio")
public class RomaneioController {

    private final RomaneioService romaneioService;

    @Autowired
    public RomaneioController(RomaneioService romaneioService) {
        this.romaneioService = romaneioService;
    }

    @GetMapping(value = "/findRomaneio/{codigo}")
    public RomaneioDto findRomaneio(@PathVariable(value = "codigo") @ApiParam Long codigo) {
        return romaneioService.findRomaneioByCodigo(codigo);
    }

    @PostMapping("/insertProduto/{codigoRomaneio}/{codigoDeBarras}")
    public void insertProdutoRomaneio(@PathVariable(value="codigoRomaneio") Long codigoRomaneio, @PathVariable(value="codigoDeBarras") Long codigoDeBarras) {
        romaneioService.insertProdutoRomaneio(codigoRomaneio, codigoDeBarras);
    }

    @PostMapping(value = "/generateDivergencia/{codigoRomaneio}")
    public void generateDivergenciaRomaneio(@PathVariable(value = "codigoRomaneio") @ApiParam Long codigoRomaneio) {
        romaneioService.generateDivergenciaRomaneio(codigoRomaneio);
    }
}
