package br.com.collection.dto;

public class ItemDivergenteDto {

    private Long codigoProduto;
    private String descricao;
    private Long codigoDeBarras;
    private Long quantidadeNota;
    private Long quantidadeRomaneio;

    public ItemDivergenteDto (Long codigoProduto, String descricao, Long codigoDeBarras, Long quantidadeNota, Long quantidadeRomaneio){
        this.codigoProduto = codigoProduto;
        this.descricao = descricao;
        this.codigoDeBarras = codigoDeBarras;
        this.quantidadeNota = quantidadeNota;
        this.quantidadeRomaneio = quantidadeRomaneio;
    }

    public Long getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(Long codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCodigoDeBarras() {
        return codigoDeBarras;
    }

    public void setCodigoDeBarras(Long codigoDeBarras) {
        this.codigoDeBarras = codigoDeBarras;
    }

    public Long getQuantidadeNota() {
        return quantidadeNota;
    }

    public void setQuantidadeNota(Long quantidadeNota) {
        this.quantidadeNota = quantidadeNota;
    }

    public long getQuantidadeRomaneio() {
        return quantidadeRomaneio;
    }

    public void setQuantidadeRomaneio(long quantidadeRomaneio) {
        this.quantidadeRomaneio = quantidadeRomaneio;
    }
}
