package br.com.collection.dto;

public class RomaneioDto {

    private Long codigo;
    private String status;
    private String descricao;
    private String nomeFornecedor;

    public RomaneioDto (Long codigo, String status, String descricao, String nomeFornecedor)
    {
        this.codigo = codigo;
        this.status = status;
        this.descricao = descricao;
        this.nomeFornecedor = nomeFornecedor;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNomeFornecedor() {
        return nomeFornecedor;
    }

    public void setNomeFornecedor(String nomeFornecedor) {
        this.nomeFornecedor = nomeFornecedor;
    }
}
