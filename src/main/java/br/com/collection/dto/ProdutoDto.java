package br.com.collection.dto;

public class ProdutoDto {

    private Long codigoProduto;
    private String descricao;
    private String descred;
    private Long quantidadePorCaixa;
    private Long quantidadePorEmbalagem;


    public ProdutoDto (Long codigoProduto, String descricao, String descred, Long quantidadePorCaixa, Long quantidadePorEmbalagem){
        this.codigoProduto = codigoProduto;
        this.descricao = descricao;
        this.descred = descred;
        this.quantidadePorCaixa = quantidadePorCaixa;
        this.quantidadePorEmbalagem = quantidadePorEmbalagem;
    }

    public Long getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(Long codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescred() {
        return descred;
    }

    public void setDescred(String descred) {
        this.descred = descred;
    }

    public Long getQuantidadePorCaixa() {
        return quantidadePorCaixa;
    }

    public void setQuantidadePorCaixa(Long quantidadePorCaixa) {
        this.quantidadePorCaixa = quantidadePorCaixa;
    }

    public Long getQuantidadePorEmbalagem() {
        return quantidadePorEmbalagem;
    }

    public void setQuantidadePorEmbalagem(Long quantidadePorEmbalagem) {
        this.quantidadePorEmbalagem = quantidadePorEmbalagem;
    }

}
