package br.com.collection.service;

import br.com.collection.dto.ProdutoDto;
import br.com.collection.dto.RomaneioDto;
import br.com.collection.repository.ProdutoRepository;
import br.com.collection.repository.RomaneioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RomaneioService {

    private final RomaneioRepository romaneioRepository;
    private final ProdutoRepository produtoRepository;

    @Autowired
    public RomaneioService(RomaneioRepository romaneioRepository, ProdutoRepository produtoRepository) {
        this.romaneioRepository = romaneioRepository;
        this.produtoRepository = produtoRepository;
    }

    public RomaneioDto findRomaneioByCodigo(Long codigo) {
        return romaneioRepository.findRomaneioByCodigo(codigo);
    }

    public void insertProdutoRomaneio(Long codigoRomaneio, Long codigoDeBarras) {

        ProdutoDto produtoDto = produtoRepository.findProdutoByCodigoDeBarra(codigoDeBarras);

        romaneioRepository.insertProdutoRomaneio(codigoRomaneio, codigoDeBarras, produtoDto);

    }

    public void generateDivergenciaRomaneio(Long codigoRomaneio) {
        romaneioRepository.generateDivergenciaRomaneio(codigoRomaneio);
    }


}
