package br.com.collection.service;

import br.com.collection.dto.ItemDivergenteDto;
import br.com.collection.repository.ItemDivergenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemDivergenteService {

    private final ItemDivergenteRepository itemDivergenteRepository;

    @Autowired
    public ItemDivergenteService(ItemDivergenteRepository itemDivergenteRepository) {
        this.itemDivergenteRepository = itemDivergenteRepository;
    }

    public List<ItemDivergenteDto> findItemDivergenteByRomaneio(Long codigoRomaneio){
        return itemDivergenteRepository.findItensDivergentes(codigoRomaneio);
    }
}
