package br.com.collection.repository;

import br.com.collection.dto.ProdutoDto;
import br.com.collection.dto.RomaneioDto;
import br.com.collection.util.MapBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Map;

@Repository
public class ProdutoRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProdutoRepository.class);

    private final TransactionTemplate transactionTemplate;

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public ProdutoRepository(TransactionTemplate transactionTemplate, NamedParameterJdbcTemplate jdbcTemplate) {
        this.transactionTemplate = transactionTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

    public ProdutoDto findProdutoByCodigoDeBarra(Long codigoDeBarra){

        String sql = "select p.pro_codigo, p.pro_descred, p.pro_descricao, coalesce(p.pro_qtd_embindustrial,1) Qtd_emb, "+
                " coalesce(bar_qtd_embalagem,1) as qtd_embalagem "+
                " from codigobarras c " +
                " inner join produtos p on (p.pro_codigo = c.pro_codigo) " +
                " where c.bar_codigobarra = :ean";

        Map<String, Object> params = new MapBuilder<String, Object>().build();
        params.put("ean", codigoDeBarra);


        return this.jdbcTemplate.queryForObject(sql,params, (rs, i) -> {
            return new ProdutoDto(
                    rs.getLong("pro_codigo"),
                    rs.getString("pro_descricao"),
                    rs.getString("pro_descred"),
                    rs.getLong("Qtd_emb"),
                    rs.getLong("qtd_embalagem"));
        });
    }
}
