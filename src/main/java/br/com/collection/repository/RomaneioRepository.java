package br.com.collection.repository;

import br.com.collection.dto.ProdutoDto;
import br.com.collection.dto.RomaneioDto;
import br.com.collection.util.MapBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.support.TransactionTemplate;

import javax.transaction.Transactional;
import java.util.Map;

@Repository
public class RomaneioRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(RomaneioRepository.class);

    private static final String USUARIO = "COLETOR";

    private final TransactionTemplate transactionTemplate;

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public RomaneioRepository(TransactionTemplate transactionTemplate, NamedParameterJdbcTemplate jdbcTemplate) {
        this.transactionTemplate = transactionTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }


    public RomaneioDto findRomaneioByCodigo(Long codigo){

        String sql = " select r.roe_codigo, coalesce(r.roe_status,'A') roe_status, r.roe_descricao, " +
                " f.for_razao as Nome_fornecedor "+
                " from romaneio_entrada r "+
                " left join Fornecedores f on (f.for_codigo = r.for_codigo) " +
                " where r.roe_codigo = :roe_codigo";

        Map<String, Object> params = new MapBuilder<String, Object>().build();
        params.put("roe_codigo", codigo);

       return this.jdbcTemplate.queryForObject(sql,params, (rs, i) -> {
            return new RomaneioDto(
                    rs.getLong("roe_codigo"),
                    rs.getString("roe_status"),
                    rs.getString("roe_descricao"),
                    rs.getString("Nome_fornecedor"));
        });
    }

    @Transactional
    public void insertProdutoRomaneio(Long codigoRomaneio, Long codigoDeBarras, ProdutoDto produto){

        String sql = "execute procedure prc_insereItemrom(:roe_codigo, :produto, :ean, :qtd, :qtd_emb, :usuario)";

        SqlParameterSource namedParameters = new MapSqlParameterSource();
        ((MapSqlParameterSource) namedParameters).addValue("roe_codigo", codigoRomaneio);
        ((MapSqlParameterSource) namedParameters).addValue("produto", produto.getCodigoProduto());
        ((MapSqlParameterSource) namedParameters).addValue("ean", codigoDeBarras);
        ((MapSqlParameterSource) namedParameters).addValue("qtd", produto.getQuantidadePorCaixa());
        ((MapSqlParameterSource) namedParameters).addValue("qtd_emb", produto.getQuantidadePorEmbalagem());
        ((MapSqlParameterSource) namedParameters).addValue("usuario", USUARIO);


        jdbcTemplate.update(sql, namedParameters);
    }

    @Transactional
    public void generateDivergenciaRomaneio(Long codigoRomaneio){

        String sql = "execute procedure prc_divergeromaneio(:romaneio, :usuario)";

        SqlParameterSource namedParameters = new MapSqlParameterSource();
        ((MapSqlParameterSource) namedParameters).addValue("romaneio", codigoRomaneio);
        ((MapSqlParameterSource) namedParameters).addValue("usuario", USUARIO);

        jdbcTemplate.update(sql, namedParameters);
    }
}
