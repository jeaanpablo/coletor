package br.com.collection.repository;

import br.com.collection.dto.ItemDivergenteDto;
import br.com.collection.dto.RomaneioDto;
import br.com.collection.util.MapBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.Map;

@Repository
public class ItemDivergenteRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemDivergenteRepository.class);

    private static final String USUARIO = "COLETOR";

    private final TransactionTemplate transactionTemplate;

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public ItemDivergenteRepository(TransactionTemplate transactionTemplate, NamedParameterJdbcTemplate jdbcTemplate) {
        this.transactionTemplate = transactionTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<ItemDivergenteDto> findItensDivergentes (Long codigoRomaneio){

        String sql = " select i.pro_codigo, p.pro_descricao, i.itd_ean, i.itd_nota_qtd, i.itd_rom_qtd from item_divergenciarom i "+
                " left join produtos p on (p.pro_codigo = i.pro_codigo) "+
                " where i.roe_codigo = :roe_codigo"+
                " group by i.pro_codigo, p.pro_descricao, i.itd_ean, i.itd_nota_qtd, i.itd_rom_qtd";

        Map<String, Object> params = new MapBuilder<String, Object>().build();
        params.put("roe_codigo", codigoRomaneio);

        return this.jdbcTemplate.query(sql,params, (rs, i) -> {
            return new ItemDivergenteDto(
                    rs.getLong("pro_codigo"),
                    rs.getString("pro_descricao"),
                    rs.getLong("itd_ean"),
                    rs.getLong("itd_nota_qtd"),
                    rs.getLong("itd_rom_qtd"));
        });

    }
}
